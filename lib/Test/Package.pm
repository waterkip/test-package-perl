package Test::Package;
use warnings;
use strict;

our $VERSION = 1.0_1;

=head1 NAME

Test::Package - A package tester

=head1 SYNOPSIS

In your t/000-package.t file:

    use Test::Package;

    all_package_files_ok();

=cut

use Pod::Elemental;
use Test::Compile;
use Test::DistManifest;
use Test::More;
use Test::Pod::Coverage;
use Test::Pod;

use base 'Exporter';

our @EXPORT = qw(all_package_files_ok);

=head1 METHODS

=head2 all_package_files_ok

all_package_files_ok();

all_package_files_ok(
    pm => [qw(lib blib)],
    pl => [qw(bin scripts)],
    skip => {
        pm           => 0,
        pl           => 0,
        pod          => 1,
        pod_coverage => 1,
        eupl         => 0,
    },
);

Tests all the relevant items of a Perl package:

=over

=item skip

An hashref which defines which test to skip.
You can skip all the test, except for manifest testing, although you may want to
rethink usage of this module if you want to skip everything.

=over

=item pl

Skip Perl scripts tests

=item pm

Skip Perl module tests

=item pod

Skip POD tests

=item pod_coverage

Skip POD coverage tests

=item eupl

Skip EUPL license checks. This is skipped by default. EUPL projects need license
information in every file. This test checks to see if there is a license block,
it does not check the actual license text itself.

=back

=item pm

An arrayref of which directories to check for compile testing modules.

=item pl

An arrayref of which directories to check for compile testing of scripts.

=back

=cut

sub all_package_files_ok {
    my %args = @_;

    if (!exists $args{skip}) {
        $args{skip} = {};
    }

    if (!defined $args{pm}) {
        $args{pm} = ['blib', 'lib'];
    }
    my @modules;
    push(@modules, all_pm_files(@{ $args{pm} }));

    if (!defined $args{pl}) {
        $args{pl} = ['bin', 'script', 'dev-bin'];
    }
    my @scripts;
    push(@scripts, all_pl_files(@{ $args{pl} }));

    my @pods;
    if (!defined $args{pod}) {
        push(@pods, @modules, @scripts);
    }
    else {
        push(@pods, all_pod_files(@{ $args{pod} }));
    }

    subtest 'Manifest tests' => sub {
        manifest_ok();
    };

    subtest 'Compile modules tests' => sub {
        if ($args{skip}{pm}) {
            plan skip_all => 'Skipping module compile tests on request';
        }
        else {
            all_pm_files_ok(@modules);
        }
    };

    subtest 'Compile script tests' => sub {
        if ($args{skip}{pl}) {
            plan skip_all => 'Skipping script files tests on request';
        }
        else {
            if (!@scripts) {
                plan skip_all => 'No pl files found';
            }
            else {
                all_pl_files_ok(@scripts);
            }
        }
    };

    subtest 'POD compile tests' => sub {
        if ($args{skip}{pod}) {
            plan skip_all => 'Skipping pod compile tests on request';
        }
        else {
            all_pod_files_ok(@pods);
        }
    };

    subtest 'POD coverage tests' => sub {
        if ($args{skip}{pod_coverage}) {
            plan skip_all => 'Skipping pod coverage tests on request';
        }
        else {
            all_pod_coverage_ok('t/lib');
        }
    };

    subtest 'EUPL license coverage' => sub {
        if (!exists $args{skip}{eupl} or $args{skip}{eupl}) {
            plan skip_all => 'Skipping EUPL license coverage';
        }
        else {
            foreach (@pods) {
                eupl_ok($_);
            }

        }
    };

    done_testing();
}

=head2 eupl_ok

Test if a file contains an EUPL license block

=cut

sub eupl_ok {
    my $file     = shift;
    my $document = Pod::Elemental->read_file($file);

    my $c = 0;
    my $l = 0;
    foreach my $child (@{ $document->children }) {
        if (ref $child eq 'Pod::Elemental::Element::Generic::Command') {
            my $content = $child->content;
            chomp($content);
            if ($content eq 'COPYRIGHT and LICENSE') {
                $c = 1;
                $l = 1;
            }
            elsif ($content eq 'LICENSE') {
                $l = 1;
            }
            elsif ($content eq 'COPYRIGHT') {
                $c = 1;
            }
        }
    }
    my $ok = 0;
    if ($c == 1 && $l == 1) {
        my $ok = 1;
    }
    elsif ($l == 0) {
        diag "$file: No LICENSE block found";
    }
    elsif ($c == 0) {
        diag "$file: No COPYRIGHT block found";
    }
    return ok($ok, "$file: EUPL license found");
}

1;

__END__

=head1 SEE ALSO

This module uses the following modules, in case you want to test everything in
seperate test files you may want to have a look at these

=over

=item L<Test::Compile>

=item L<Test::More>

=item L<Test::Pod::Coverage>

=item L<Test::Pod>

=item L<Test::DistManifest>

=back
